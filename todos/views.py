from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm
# Create your views here.


def todo_list(request):
    lists = TodoList.objects.all()
    context = {
        "todo_list": lists,
    }
    return render(request, "todos/list.html", context)


def show_list(request, id):
    list = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list_object": list,
    }
    return render(request, "todos/detail.html", context)


def create_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list_instance = form.save()
            return redirect("todo_list_detail", id=list_instance.id)
    else:
        form = TodoListForm()

    context = {
        "form": form
    }
    return render(request, "todos/create.html", context)


def update_list(request, id):
    list = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=list)
        if form.is_valid():
            list_instance = form.save()
            return redirect("todo_list_detail", id=list_instance.id)
    else:
        form = TodoListForm(instance=list)

    context = {
        "form": form,
        "todo_list_object": list,
    }
    return render(request, "todos/update.html", context)


def delete_list(request, id):
    list = TodoList.objects.get(id=id)
    if request.method == "POST":
        list.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def create_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()

    context = {
        "form": form
    }
    return render(request, "todos/newitem.html", context)


def update_item(request, id):
    item = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=item)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm(instance=item)

    context = {
        "form": form,
        "todo_item_object": item,
    }
    return render(request, "todos/edititem.html", context)
